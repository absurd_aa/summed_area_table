**Summed-area Data Structure**

## Summary

This project consists of the implementation of a data structure (“pixel sum”) which is described in many sources ([Wiki](https://en.wikipedia.org/wiki/Summed-area_table), [Fast Template Matching](http://scribblethink.org/Work/nvisionInterface/vi95_lewis.pdf) etc.)

---

## Specification

1. The code should produce correct and exact output for all valid input data.
2. There should be sufficient testing code to demonstrate the robustness and handling of
obvious edge cases for all of the functions.
3. The code should not crash or leak memory (assuming valid input is provided) under any
circumstances.
4. All non-obvious parts of your code are properly commented.
5. The algorithm chosen is O(1) and obvious code optimizations have been made. It should be
noted here that there are tons of tricks to speed up this particular algorithm. We don’t expect
you to implement all of them.
6. The general style and readability of the code.

---

## Structure of code

* PixelSumLib

This is an implementation of the needed PixelSum class, but also of NaivePixelSum which is a naive version of data structure. This class is used as a reference in the test application.

* PixelSumTestApp

This is a testing application that according to task description is written without testing frameworks. 
It provides 3 types of tests: 
Unit Test for the class interface, that very briefly checks that all needed class functionality work correctly.
**Unit Tests** for PixelSum and NaivePixelSum check that both classes provide correct output for some precalculated cases and works correctly for border cases.
**Performance Tests** demonstrate time complexity and test both Classes on data with different sizes.

---

## How to run

Project files for Windows (Visual Studio) and Linux (Cmake) are attached.

To build and run it on Linux machine Cmake(>=3.16) should be installed.

* cmake CMakeLists.txt

* make 

* ./PixelSumTestApp/PixelSumTestApp

---

## Essay

* Typical buffers in our use cases are extremely sparse matrices, i.e., the vast majority of the pixels are zero. How would you exploit this to optimize the implementation?

We can have advantages of a small number of nonzero elements by using special technics for sparse matrix storing. Most of the different popular storage formats employ the same basic technique, store all non-zero elements of the matrix into a linear array and provide auxiliary arrays to describe the locations of the non-zero elements in the original matrix [*](https://scc.ustc.edu.cn/zlsc/sugon/intel/mkl/mkl_manual/GUID-9FCEB1C4-670D-4738-81D2-F378013412B0.htm#:~:text=Storage%20Formats%20for%20the%20Direct,they%20appear%20in%20the%20walk.). We can try Compressed sparse row format described [here](https://en.wikipedia.org/wiki/Sparse_matrix#cite_note-6). It will save time for summed-area table initialization. We also can find a compromise between storage size and operation speed and try to use advanced sparse matrix storage for the summed-area table. 
Finding some matrix features (like Band matrices) can also help us to find the optimal solution.

* What changes to the code and the API need to be made if buffer dimensions can be >= 4096 x 4096? Also, what would happen if instead of 8 bits per pixel we have 16 bits per pixel?

The possible maximum value for **unsigned int getPixelSum(int x0, int y0, int x1, int y1) const** is 4096 * 4096 * 256 (unsigned char max) = 2^32 which fits unsigned int with the same [limit](https://docs.microsoft.com/en-us/cpp/c-language/cpp-integer-limits?view=vs-2019). So every change of format (buffer dimension or pixel size) requires a change of getPixelSum signature for return type with a bigger limit and may require getNonZeroCount changes (for a significant increase of dimensions). Pixel format change obviously needs constructor signature adjustment as well.

* What would change if we needed to find the maximum value inside the search window (instead of the sum or average)?

Unfortunately, we cannot simply adopt our approach in this case, because additivity does not help us in searching for maximum value. According to articles described different Two Dimensional Range Minimum Queries approaches, we can find a trade-off between [(O(N log∗ N ) preprocessing time and space and O(1) query time)](https://www.researchgate.net/publication/221314133_Two-Dimensional_Range_Minimum_Queries) and [something less fast, but also less memory consuming](https://www.cs.au.dk/~gerth/papers/esa12.pdf)

* How would multi-threading change your implementation? What kinds of performance benefits would you expect?

Possible benefits depend on hardware and our design. Proper machine resources with multi-threading design of code can help us to split buffer into several chunks and decrease constructor time.

* Outline shortly how the class would be implemented on a GPU (choose any relevant technology, e.g. CUDA, OpenCL).

I read several articles on the web for our task with CUDA technology, [this one](https://www.nmr.mgh.harvard.edu/~berkin/Bilgic_2010_Integral_Image.pdf) contains code as well and based on [this approach](https://developer.nvidia.com/gpugems/gpugems3/part-vi-gpu-computing/chapter-39-parallel-prefix-sum-scan-cuda).

The main idea is quite simple: split the matrix into chunks, send them to devices, calculate the sum of blocks, and then store them into the result array. The main challenge is to find the optimal design of buffer blocks segmentation and threads grid suitable for concrete hardware