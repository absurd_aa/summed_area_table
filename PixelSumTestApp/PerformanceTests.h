#pragma once
#include "TestUtils.h"

namespace VarjoTest
{
    class PerformanceTests
    {
    public:
        static void run(int maxSize);

        //try to calculate characteristics for a relatively big window
        static void referenceProcessorTest(std::unique_ptr<AbstractPixelSum>& refProcessor, std::unique_ptr<AbstractPixelSum>& processor);

        //check operations for max possible buffer
        static void checkBigBufferCase(std::unique_ptr<AbstractPixelSum>& processor);
    private:
        static TestData createSquareAreaTestData(const std::vector<unsigned char>& data, int len);
        static TestData createSquareAreaTestData(unsigned char element, int len);
    };

}

