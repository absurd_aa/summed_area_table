#include "TestUtils.h"

#include <iostream>

#include "PixelSumLib/PixelSum.h"
#include "PixelSumLib/NaivePixelSum.h"

using namespace VarjoTest;

namespace
{
    std::unique_ptr<AbstractPixelSum> initProcessor(const ProcessorType type, const TestData& testData)
    {
        std::unique_ptr<AbstractPixelSum> processor;

        const auto start = std::chrono::system_clock::now();
        if (type == ProcessorType::Main)
        {
            processor = std::make_unique<PixelSum>(testData.data.data(), testData.widthX, testData.heightY);
        }
        else
            processor = std::make_unique<NaivePixelSum>(testData.data.data(), testData.widthX, testData.heightY);
        std::cout << processor->processorName() << " processor init time, ms: " << TestUtils::timeDuration(start) << ". ";
        return processor;
    }

    void printTestInfo(const std::string& testName, const int w, const int h)
    {
        std::cout << testName << ". Size: " << w * h << "(" << w << " * " << h << "). ";
    }
}


bool TestUtils::runTestCase(const std::string& testName, const ProcessorType type, const TestData& testData,
                            void checkFunc(std::unique_ptr<AbstractPixelSum>& proc))
{
    printTestInfo(testName, testData.widthX, testData.heightY);
    auto processor = initProcessor(type, testData);

    const auto startTest = std::chrono::system_clock::now();
    auto result = true;
    try
    {
        checkFunc(processor);
    }
    catch (std::exception& e)
    {
        std::cout << std::endl << "Fail :" << e.what();
        result = false;
    }
    
    std::cout << "Test time, ms: " << timeDuration(startTest);
    std::cout << std::endl << "Result: " << std::boolalpha << result << std::endl << std::endl;
    return result;
}

bool TestUtils::runTwoProcessorsTestCase(const std::string& testName, const TestData& testData, void checkFunc(std::unique_ptr<AbstractPixelSum>& ref,
    std::unique_ptr<AbstractPixelSum>& proc))
{
    printTestInfo(testName, testData.widthX, testData.heightY);
    auto refProcessor = initProcessor(ProcessorType::Naive, testData);
    auto mainProcessor = initProcessor(ProcessorType::Main, testData);

    auto result = true;
    try
    {
        checkFunc(refProcessor, mainProcessor);
    }
    catch (std::exception& e)
    {
        std::cout << std::endl << "Fail :" << e.what();
        result = false;
    }

    std::cout << std::endl << "Result: " << std::boolalpha << result << std::endl << std::endl;
    return result;
}

//time in ms
double TestUtils::timeDuration(const std::chrono::system_clock::time_point& start)
{
    return std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - start).count() / 1000.0;
}

void TestUtils::expectNear(const double expected, const double value, const double eps)
{
    if (std::abs(value - expected) <= eps)
        return;
    throw std::logic_error("Expected: " + std::to_string(expected) + ". Result: " + std::to_string(value) + ". ");
}
