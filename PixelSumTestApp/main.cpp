#include "UnitTests.h"
#include "PerformanceTests.h"

using namespace VarjoTest;

int main(const int argc, char* argv[])
{
    //tun tests on PixelSum interface
    UnitTests::checkInterface();

    //run unit tests for naive PixelSum, this processor will be used as a reference for more complicated tests
    UnitTests::run(ProcessorType::Naive, "Naive Processor Unit Tests");

    //run the same tests for main PixelSum
    UnitTests::run(ProcessorType::Main, "Main Processor Unit Tests");

    //run tests on bigger data (from 2 to 2048)
    PerformanceTests::run(2048);
}