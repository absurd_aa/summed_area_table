#include "UnitTests.h"

#include <iostream>
#include <numeric>

#include "TestUtils.h"
#include "PixelSumLib/PixelSum.h"
#include "PixelSumLib/Utils.h"

using namespace VarjoTest;

void UnitTests::run(const ProcessorType type, const std::string& setName)
{
    std::cout << std::endl << setName << " tests started" << std::endl;
    std::vector<bool> testResults;

    testResults.push_back(TestUtils::runTestCase("Description case 1", type, createTinyDataSample(), checkDescriptionCase));
    testResults.push_back(TestUtils::runTestCase("Description case with data of ones", type, createTinyDataSampleOfOnes(), 
        checkDescriptionCaseWithDataOfOnes));
    testResults.push_back(TestUtils::runTestCase("Order of arguments", type, createTinyDataSample6x7(), checkOrderOfArguments));
    testResults.push_back(TestUtils::runTestCase("Out of borders", type, createTinyDataSample6x7(), checkOutOfBorders));
    testResults.push_back(TestUtils::runTestCase("Single element", type, createOneElementTestData(), checkOneElementCase));
    testResults.push_back(TestUtils::runTestCase("Single column", type, createOneColumnTestData(), checkOneColumnCase));

    std::cout << "Passed tests: " << std::accumulate(testResults.begin(), testResults.end(), 0) << " / " << testResults.size() <<
        std::endl << std::endl;
}


TestData UnitTests::createTinyDataSample()
{
    return TestData
    {
        {0, 4, 0, 2, 1, 0}, 3, 2
    };
}


void UnitTests::checkDescriptionCase(std::unique_ptr<AbstractPixelSum>& processor)
{
    TestUtils::expectNear(7, processor->getPixelSum(0, 0, 2, 1));
    TestUtils::expectNear(3, processor->getNonZeroCount(0, 0, 2, 1));
    TestUtils::expectNear(2.333, processor->getNonZeroAverage(0, 0, 2, 1), 1e-3);
    TestUtils::expectNear(1.166, processor->getPixelAverage(0, 0, 2, 1), 1e-3);
}


TestData UnitTests::createTinyDataSampleOfOnes()
{
    return TestData
    {
        {1, 1, 1, 1}, 2, 2
    };
}


void UnitTests::checkDescriptionCaseWithDataOfOnes(std::unique_ptr<AbstractPixelSum>& processor)
{
    TestUtils::expectNear(0.111, processor->getPixelAverage(1, 1, 3, 3), 1e-3);
    TestUtils::expectNear(0.25, processor->getPixelAverage(0, 0, 3, 3), 1e-3);
}


TestData UnitTests::createTinyDataSample6x7()
{
    return TestData
    {
        {0, 4, 0, 2, 1, 0,
        1, 1, 1, 1, 3, 2,
        1, 8, 0, 4, 0, 2,
        1, 0, 1, 1, 1, 1,
        3, 2, 1, 8, 0, 4,
        0, 2, 1, 0, 1, 1,
        1, 1, 3, 2, 1, 8}, 6, 7
    };
}


void UnitTests::checkOrderOfArguments(std::unique_ptr<AbstractPixelSum>& processor)
{
    TestUtils::expectNear(processor->getPixelSum(3, 4, 5, 6), processor->getPixelSum(5, 4, 3, 6));
}


void UnitTests::checkOutOfBorders(std::unique_ptr<AbstractPixelSum>& processor)
{
    TestUtils::expectNear(0, processor->getPixelSum(6, 7, 8, 9));
    TestUtils::expectNear(0, processor->getNonZeroCount(6, 7, 8, 9));
    TestUtils::expectNear(0, processor->getNonZeroAverage(6, 7, 8, 9), 1e-3);
    TestUtils::expectNear(0, processor->getPixelAverage(6, 7, 8, 91), 1e-3);

    //negative indices
    TestUtils::expectNear(4, processor->getPixelSum(-5, -5, 1, 0));
    TestUtils::expectNear(4.0 / 42, processor->getPixelAverage(-5, -5, 1, 0), 1e-3);
}


TestData UnitTests::createOneElementTestData()
{
    return TestData{ {5}, 1, 1 };
}


void UnitTests::checkOneElementCase(std::unique_ptr<AbstractPixelSum>& processor)
{
    TestUtils::expectNear(5, processor->getPixelSum(0, 0, 9, 9));
    TestUtils::expectNear(1, processor->getNonZeroCount(0, 0, 9, 9));
    TestUtils::expectNear(5, processor->getNonZeroAverage(0, 0, 9, 9), 1e-3);
    TestUtils::expectNear(0.05, processor->getPixelAverage(0, 0, 9, 9), 1e-3);
}


TestData UnitTests::createOneColumnTestData()
{
    return TestData{ {1, 0, 1, 3}, 1, 4 };
}


void UnitTests::checkInterface()
{
    std::cout << "Test interface PixelSum" << std::endl;
    const auto testData = createTinyDataSample6x7();
    const auto testDataSum = std::accumulate(testData.data.begin(), testData.data.end(), 0.0);
    const auto mainProcessor = PixelSum(testData.data.data(), 6, 7);
    const auto newProcessor = PixelSum(mainProcessor);
    const auto processor1 = newProcessor;

    const auto windowSize = 9;
    auto result = true;
    try
    {
        TestUtils::expectNear(testDataSum, newProcessor.getPixelSum(0, 0, windowSize, windowSize));

        const auto numberOfZeros = 9;
        const auto nonZeroCount = static_cast<double>(testData.data.size()) - numberOfZeros;
        TestUtils::expectNear(nonZeroCount, newProcessor.getNonZeroCount(0, 0, windowSize, windowSize));
        TestUtils::expectNear(testDataSum / nonZeroCount, processor1.getNonZeroAverage(0, 0, windowSize, windowSize), 1e-3);

        const auto fullSearchArea = Utils::calcInclusiveArea(windowSize + 1, windowSize + 1);
        TestUtils::expectNear(testDataSum / fullSearchArea, processor1.getPixelAverage(0, 0, windowSize, windowSize), 1e-3);
    }
    catch (std::exception& e)
    {
        std::cout << std::endl << "Fail :" << e.what() << std::endl;
        result = false;
    }
    std::cout << "Result: " << std::boolalpha << result << std::endl << std::endl;
}


void UnitTests::checkOneColumnCase(std::unique_ptr<AbstractPixelSum>& processor)
{
    TestUtils::expectNear(1, processor->getPixelSum(0, 0, 9, 0));
    TestUtils::expectNear(5, processor->getPixelSum(0, 0, 9, 9));
    TestUtils::expectNear(3, processor->getNonZeroCount(0, 0, 9, 9));
    TestUtils::expectNear(5.0 / 3, processor->getNonZeroAverage(0, 0, 9, 9), 1e-3);
    TestUtils::expectNear(0.05, processor->getPixelAverage(0, 0, 9, 9), 1e-3);
}
