#pragma once
#include <string>
#include <memory>
#include <vector>
#include <chrono>

#include "PixelSumLib/AbstractPixelSum.h"

namespace VarjoTest
{
    enum class ProcessorType { Naive, Main };

    struct TestData
    {
        std::vector<unsigned char> data;
        int widthX;
        int heightY;
    };

    // according to the rules 3rd parties code is not acceptable, so this is tiny example of test lib
    class TestUtils
    {
    public:
        static bool runTestCase(const std::string& testName, ProcessorType type, const TestData& testData,
            void checkFunc(std::unique_ptr<AbstractPixelSum>& proc));

        static bool runTwoProcessorsTestCase(const std::string& testName, const TestData& testData,
            void checkFunc(std::unique_ptr<AbstractPixelSum>& ref, std::unique_ptr<AbstractPixelSum>& proc));

        static double timeDuration(const std::chrono::system_clock::time_point& start);

        static void expectNear(double expected, double value, double eps = 1e-4);
    };
}


