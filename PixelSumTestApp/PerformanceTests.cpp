#include "PerformanceTests.h"

#include <iostream>
#include <chrono>
#include <numeric>
#include <climits>

#include "PixelSumLib/Utils.h"

using namespace VarjoTest;

void PerformanceTests::run(const int maxSize)
{
    std::cout << std::endl << "Performance tests started" << std::endl;
    std::vector<bool> testResults;

    for(auto i = 2; i <= maxSize;  i *= 2)
        testResults.push_back(TestUtils::runTwoProcessorsTestCase("Size " + std::to_string(i), createSquareAreaTestData({1, 2, 3, 4}, i), referenceProcessorTest));

    const auto maxPossibleSize = 4096;
    testResults.push_back(TestUtils::runTestCase("Big buffer case", ProcessorType::Main, createSquareAreaTestData(UCHAR_MAX, maxPossibleSize), checkBigBufferCase));

    std::cout << "Passed tests: " << std::accumulate(testResults.begin(), testResults.end(), 0) << " / " << testResults.size() << std::endl << std::endl;
}

void PerformanceTests::referenceProcessorTest(std::unique_ptr<AbstractPixelSum>& refProcessor, std::unique_ptr<AbstractPixelSum>& processor)
{
    const auto windowSize = 4000;
    //expected the same time complexity, so it is possible to calc average
    const auto t1 = std::chrono::system_clock::now();
    const auto refPixelSum = refProcessor->getPixelSum(0, 0, windowSize, windowSize);
    const auto refNonZeroCount = refProcessor->getNonZeroCount(0, 0, windowSize, windowSize);
    const auto refNonZeroAverage = refProcessor->getNonZeroAverage(0, 0, windowSize, windowSize);
    const auto refPixelAverage = refProcessor->getPixelAverage(0, 0, windowSize, windowSize);

    const auto refTime = TestUtils::timeDuration(t1) / 4;

    const auto t2 = std::chrono::system_clock::now();
    TestUtils::expectNear(refPixelSum, processor->getPixelSum(0, 0, windowSize, windowSize));
    TestUtils::expectNear(refNonZeroCount, processor->getNonZeroCount(0, 0, windowSize, windowSize));
    TestUtils::expectNear(refNonZeroAverage, processor->getNonZeroAverage(0, 0, windowSize, windowSize));
    TestUtils::expectNear(refPixelAverage, processor->getPixelAverage(0, 0, windowSize, windowSize));
    const auto mainTime = TestUtils::timeDuration(t2) / 4;

    std::cout << "Operation time, ms. Naive processor: " << refTime  << ". Main processor: " << mainTime << std::endl;
}


void PerformanceTests::checkBigBufferCase(std::unique_ptr<AbstractPixelSum>& processor)
{
    const short windowSize = 4000;
    const auto areaSize = static_cast<double>(Utils::calcInclusiveArea(0, 0, windowSize, windowSize));
    TestUtils::expectNear( areaSize * UCHAR_MAX, processor->getPixelSum(0, 0, windowSize, windowSize));
    TestUtils::expectNear(areaSize, processor->getNonZeroCount(0, 0, windowSize, windowSize));
    TestUtils::expectNear(UCHAR_MAX, processor->getNonZeroAverage(0, 0, windowSize, windowSize));
    TestUtils::expectNear(UCHAR_MAX, processor->getPixelAverage(0, 0, windowSize, windowSize));
}

TestData PerformanceTests::createSquareAreaTestData(const std::vector<unsigned char>& data, const int len)
{
    const auto resultDataSize = len * len;
    auto resultData = std::vector<unsigned char>(resultDataSize);
    const auto d = std::div(resultDataSize, static_cast<int>(data.size()));

    for (auto i = 0; i < d.quot; ++i)
    {
        resultData.insert(resultData.end(), data.begin(), data.end());
    }
    if (d.rem > 0)
        resultData.insert(resultData.end(), data.begin(), data.begin() + d.rem);
    return TestData
    {
        resultData, len, len
    };
}

TestData PerformanceTests::createSquareAreaTestData(const unsigned char element, const int len)
{
    const auto resultDataSize = len * len;
    const auto resultData = std::vector<unsigned char>(resultDataSize, element);
    return TestData
    {
        resultData, len, len
    };
}
