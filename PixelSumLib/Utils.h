#pragma once

namespace VarjoTest
{
    class Utils
    {
    public:
        static void reorderArguments(int& x0, int& y0, int& x1, int& y1);
        static bool adjustBordersToArea(int& x0, int& y0, int& x1, int& y1, int sizeX, int sizeY);
        static int calcInclusiveArea(int x0, int y0, int x1, int y1);
        static int calcInclusiveArea(int width, int height, int additionalShift = 0);
    };
}

