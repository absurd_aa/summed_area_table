#include "NaivePixelSum.h"

#include <numeric>
#include <algorithm>

#include "Utils.h"

using namespace VarjoTest;

NaivePixelSum::NaivePixelSum(const unsigned char* buffer, const int xWidth, const int yHeight) : AbstractPixelSum(xWidth, yHeight)
{
    _data = std::vector<unsigned char>(buffer, buffer + static_cast<size_t>(xWidth) * yHeight * sizeof(unsigned char));
}

unsigned int NaivePixelSum::getPixelSum(int x0, int y0, int x1, int y1) const
{
    Utils::reorderArguments(x0, y0, x1, y1);
    const auto isInRange = Utils::adjustBordersToArea(x0, y0, x1, y1, _xWidth, _yHeight);
    if (!isInRange)
        return 0;
    return std::accumulate(_data.begin() + at(x0, y0), _data.begin() + at(x1, y1) + 1, 0);
}

double NaivePixelSum::getPixelAverage(const int x0, const int y0, const int x1, const int y1) const
{
    return static_cast<double>(getPixelSum(x0, y0, x1, y1)) /
        Utils::calcInclusiveArea(x0, y0, x1, y1);
}

int NaivePixelSum::getNonZeroCount(int x0, int y0, int x1, int y1) const
{
    Utils::reorderArguments(x0, y0, x1, y1);
    const auto isInRange = Utils::adjustBordersToArea(x0, y0, x1, y1, _xWidth, _yHeight);
    if (!isInRange)
        return 0;
    return static_cast<int>(std::count_if(_data.begin() + at(x0, y0), _data.begin() + at(x1, y1) + 1,
                                          [](const unsigned char& obj) { return obj > 0; }));
}

double NaivePixelSum::getNonZeroAverage(const int x0, const int y0, const int x1, const int y1) const
{
    const auto count = getNonZeroCount(x0, y0, x1, y1);
    return count ? static_cast<double>(getPixelSum(x0, y0, x1, y1)) / count : 0;
}

std::string NaivePixelSum::processorName() const
{
    return "Naive";
}

size_t NaivePixelSum::at(const int x, const int y) const
{
    return static_cast<size_t>(y) * _xWidth + x;
}
