#include "PixelSum.h"
#include "Utils.h"

using namespace VarjoTest;

PixelSum::PixelSum(const unsigned char* buffer, const int xWidth, const int yHeight) : AbstractPixelSum(xWidth, yHeight)
{
    //according to task description we should copy initial buffer
    //we also do not need initial buffer for existing methods, so this buffer is local
    //data structures for buffers can be optimized during next iterations of developing process
    const auto copyBuffer = std::vector<unsigned char>(buffer, buffer + static_cast<size_t>(xWidth) * yHeight * sizeof(unsigned char));

    //additional row and column to save time while checking border conditions
    const auto bufSize = Utils::calcInclusiveArea(xWidth, yHeight, 1);

    _summedAreaTable = std::vector<unsigned int>(bufSize, 0);
    _nonZeroCountAreaTable = std::vector<int>(bufSize, 0);

    //cash values for all rectangles according to https://en.wikipedia.org/wiki/Summed-area_table
    for (auto j = 0; j < yHeight; j++)
    {
        for (auto i = 0; i < xWidth; i++)
        {
            fillBuffers(copyBuffer[at(i, j, 0)], at(i, j), at(i, j - 1),
                at(i - 1, j), at(i - 1, j - 1));
        }
    }
}

unsigned PixelSum::getPixelSum(const int x0, const int y0, const int x1, const int y1) const
{
    return calcAreaValue(x0, y0, x1, y1, _summedAreaTable);
}

double PixelSum::getPixelAverage(const int x0, const int y0, const int x1, const int y1) const
{
    return static_cast<double>(getPixelSum(x0, y0, x1, y1)) /
        Utils::calcInclusiveArea(x0, y0, x1, y1);
}

int PixelSum::getNonZeroCount(const int x0, const int y0, const int x1, const int y1) const
{
    return calcAreaValue(x0, y0, x1, y1, _nonZeroCountAreaTable);
}

double PixelSum::getNonZeroAverage(const int x0, const int y0, const int x1, const int y1) const
{
    const auto count = getNonZeroCount(x0, y0, x1, y1);
    return count > 0 ? static_cast<double>(getPixelSum(x0, y0, x1, y1)) /
        getNonZeroCount(x0, y0, x1, y1) : 0;
}

std::string PixelSum::processorName() const
{
    return "Main";
}

void PixelSum::fillBuffers(const unsigned char value, const int curInd, const int prevUpInd, const int prevLeftInd, const int prevUpLeftInd)
{
    //details are here https://en.wikipedia.org/wiki/Summed-area_table
    _summedAreaTable[curInd] = _summedAreaTable[prevUpInd] + _summedAreaTable[prevLeftInd] + value - _summedAreaTable[prevUpLeftInd];
    //same rule for non zero count
    _nonZeroCountAreaTable[curInd] = _nonZeroCountAreaTable[prevUpInd] + _nonZeroCountAreaTable[prevLeftInd] + 
        (value > 0 ? 1 : 0) - _nonZeroCountAreaTable[prevUpLeftInd];
}


//calc value from buffers according to rule Sum = D - B - C + A from https://en.wikipedia.org/wiki/Summed-area_table
template <typename T>
T PixelSum::calcAreaValue(int x0, int y0, int x1, int y1, const std::vector<T>& buffer) const
{
    //check and reorder area boundaries
    Utils::reorderArguments(x0, y0, x1, y1);
    const auto isInRange = Utils::adjustBordersToArea(x0, y0, x1, y1, _xWidth, _yHeight);
    if (!isInRange)
        return 0;

    //get values from buffer
    return buffer[at(x1, y1)] - buffer[at(x0 - 1, y1)] - buffer[at(x1, y0 - 1)] + buffer[at(x0 - 1, y0 - 1)];
}

//calc index in buffer by matrix coordinates. shift means that we have empty rows and columns for convenience
int PixelSum::at(const int x, const int y, const int shift) const
{
    return (y + shift) * (_xWidth + shift) + (x + shift);
}
