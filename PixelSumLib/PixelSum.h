#pragma once
#include <vector>
#include <string>

#include "AbstractPixelSum.h"

//----------------------------------------------------------------------------
// Class for providing fast region queries from an 8-bit pixel buffer.
// Note: all coordinates are *inclusive* and clamped internally to the borders
// of the buffer by the implementation.
//
// For example: getPixelSum(4, 8, 7, 10) gets the sum of a 4x3 region where top left
// corner is located at (4, 8) and bottom right at (7, 10). In other words
// all coordinates are _inclusive_.
//
// If the resulting region after clamping is empty, the return value for all
// functions should be 0.
//
// The width and height of the buffer dimensions < 4096 x 4096.
//----------------------------------------------------------------------------
namespace VarjoTest
{
    class PixelSum final : public AbstractPixelSum
    {
    public:
        PixelSum(const unsigned char* buffer, int xWidth, int yHeight);
        ~PixelSum(void) = default;
        PixelSum(const PixelSum&) = default;
        PixelSum& operator= (const PixelSum&) = default;
        unsigned int getPixelSum(int x0, int y0, int x1, int y1) const override;
        double getPixelAverage(int x0, int y0, int x1, int y1) const override;
        int getNonZeroCount(int x0, int y0, int x1, int y1) const override;
        double getNonZeroAverage(int x0, int y0, int x1, int y1) const override;
        std::string processorName() const override;

    private:
        void fillBuffers(unsigned char value, int curInd, int prevUpInd, int prevLeftInd, int prevUpLeftInd);
        int at(int x, int y, int shift = 1) const;
        template<typename T>
        T calcAreaValue(int x0, int y0, int x1, int y1, const std::vector<T>& buffer) const;
        std::vector<unsigned int> _summedAreaTable;
        std::vector<int> _nonZeroCountAreaTable;
    };
}
