#include "Utils.h"

#include <algorithm>

using namespace VarjoTest;

void Utils::reorderArguments(int& x0, int& y0, int& x1, int& y1)
{
    if (x1 < x0)
        std::swap(x0, x1);
    if (y1 < y0)
        std::swap(y0, y1);
}

bool Utils::adjustBordersToArea(int& x0, int& y0, int& x1, int& y1, const int sizeX, const int sizeY)
{
    if (x0 >= sizeX || y0 >= sizeY || x1 < 0 || y1 < 0)
        return false;
    x0 = std::max(x0, 0);
    x1 = std::min(sizeX - 1, x1);
    y0 = std::max(y0, 0);
    y1 = std::min(sizeY - 1, y1);
    return true;
}

int Utils::calcInclusiveArea(const int x0, const int y0, const int x1, const int y1)
{
    return (std::abs(x1 - x0) + 1) * (std::abs(y1 - y0) + 1);
}

int Utils::calcInclusiveArea(const int width, const int height, const int additionalShift)
{
    return (width + additionalShift) * (height + additionalShift);
}
