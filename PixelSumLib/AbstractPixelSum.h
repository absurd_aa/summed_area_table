#pragma once
#include <string>

namespace VarjoTest
{
    class AbstractPixelSum
    {
    public:
        AbstractPixelSum(int xWidth, int yHeight);
        ~AbstractPixelSum(void) = default;
        AbstractPixelSum(const AbstractPixelSum&) = default;
        AbstractPixelSum& operator= (const AbstractPixelSum&) = default;
        [[nodiscard]] virtual unsigned int getPixelSum(int x0, int y0, int x1, int y1) const = 0;
        [[nodiscard]] virtual double getPixelAverage(int x0, int y0, int x1, int y1) const = 0;
        [[nodiscard]] virtual int getNonZeroCount(int x0, int y0, int x1, int y1) const = 0;
        [[nodiscard]] virtual double getNonZeroAverage(int x0, int y0, int x1, int y1) const = 0;
        [[nodiscard]] virtual std::string processorName() const;
    protected:
        int _xWidth;
        int _yHeight;
    };
}

