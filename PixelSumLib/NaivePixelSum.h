#pragma once
#include <vector>
#include "AbstractPixelSum.h"

namespace VarjoTest
{
    class NaivePixelSum final: public AbstractPixelSum
    {
    public:
        NaivePixelSum(const unsigned char* buffer, int xWidth, int yHeight);
        ~NaivePixelSum(void) = default;
        NaivePixelSum(const NaivePixelSum&) = default;
        NaivePixelSum& operator= (const NaivePixelSum&) = default;
        [[nodiscard]] unsigned int getPixelSum(int x0, int y0, int x1, int y1) const override;
        [[nodiscard]] double getPixelAverage(int x0, int y0, int x1, int y1) const override;
        [[nodiscard]] int getNonZeroCount(int x0, int y0, int x1, int y1) const override;
        [[nodiscard]] double getNonZeroAverage(int x0, int y0, int x1, int y1) const override;
        [[nodiscard]] std::string processorName() const override;
    private:
        [[nodiscard]] size_t at(int x, int y) const;
        std::vector<unsigned char> _data;
    };
}

