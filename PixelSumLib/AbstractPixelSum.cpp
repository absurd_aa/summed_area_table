#include "AbstractPixelSum.h"

VarjoTest::AbstractPixelSum::AbstractPixelSum(const int xWidth, const int yHeight): _xWidth(xWidth), _yHeight(yHeight)
{
}

std::string VarjoTest::AbstractPixelSum::processorName() const
{
    return "Abstract";
}
